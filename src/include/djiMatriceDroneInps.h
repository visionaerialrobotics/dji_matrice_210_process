#include <iostream>
#include <math.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//// ROS  ///////
#include "ros/ros.h"

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneCommand.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"

//mav_msgs
#include "mav_msgs/RollPitchYawrateThrust.h"

//geometry msgs
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

//Navmsg
#include "nav_msgs/Odometry.h"

#include "eigen_conversions/eigen_msg.h"

//standard messages ROS
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"
#include "eigen_conversions/eigen_msg.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "tf_conversions/tf_eigen.h"
#include <cmath>
#include "std_msgs/Empty.h"

#include "sensor_msgs/Joy.h"

#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneSpeedsStamped.h"

#include <dji_sdk/DroneTaskControl.h>
#include <dji_sdk/SDKControlAuthority.h>
//#include <dji_sdk/QueryDroneVersion.h>
//#include <dji_sdk/SetLocalPosRef.h>
//#include "dji_sdk/dji_sdk.h" Give a weird problem

//ros time synchronizer
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//#define MPC_MODEL_INDENTIFICATION

/////////////////////////////////////////
// Class DroneCommand
//
//   Description
//
/////////////////////////////////////////
class DroneCommandROSModule : public DroneModule
{
    //Constructors and destructors
public:
    DroneCommandROSModule();
    ~DroneCommandROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
public:
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void sendSetpoints();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    ros::Subscriber CommandSubs;
    ros::Subscriber rotation_angles_subscriber;
    ros::Subscriber battery_subscriber;
    ros::Subscriber hector_slam_subscriber;
    ros::Subscriber yaw_ref_subscriber;
    ros::Subscriber Ekf_yaw_subscriber;
    ros::Subscriber activate_aerostack_control_sub;
    ros::Subscriber daltitude_command_sub;
    ros::Subscriber speed_refs_sub;
    ros::Subscriber command_roll_pitch_yawrate_thrust_sub_;

#ifdef MPC_MODEL_INDENTIFICATION
    void rcCallback(sensor_msgs::JoyConstPtr msg);
    ros::Subscriber rc_sub;
    ros::Publisher mav_rpy_thrust_pub_;
#endif


    void rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void batteryCallback(const droneMsgsROS::battery& msg);
    void hectorSlamCallback(const nav_msgs::Odometry& msg);
    void ekfyawCallback(const droneMsgsROS::dronePose& msg);
    void commandCallback(const droneMsgsROS::droneCommand::ConstPtr& msg);
    void activateAerostackControl(const std_msgs::Empty& msg);

    //synchronizing the attitude and altitude commands
    message_filters::Subscriber<droneMsgsROS::droneAutopilotCommand> drone_atti_command_sub;
    message_filters::Subscriber<droneMsgsROS::droneDAltitudeCmd> drone_sync_alti_sub;
    typedef message_filters::sync_policies::ApproximateTime<droneMsgsROS::droneAutopilotCommand, droneMsgsROS::droneDAltitudeCmd> sync_policy_atti;
    message_filters::Synchronizer<sync_policy_atti> sync_atti;
    void droneAttiAltiSyncCallback(droneMsgsROS::droneAutopilotCommandConstPtr atti_msg,
                                   droneMsgsROS::droneDAltitudeCmdConstPtr alti_msg);

    void droneRollPitchYawrateThrustCallback(const mav_msgs::RollPitchYawrateThrustConstPtr& msg);
    void droneRollPitchYawrateVerticalSpeedCallback(const mav_msgs::RollPitchYawrateThrustConstPtr& msg);


    //synchronizing the only yaw and altitude and vx_ref, vy_ref_commands
    message_filters::Subscriber<droneMsgsROS::droneAutopilotCommand> drone_atti_vel_command_sub;
    message_filters::Subscriber<droneMsgsROS::droneDAltitudeCmd> drone_sync_alti_vel_sub;
    message_filters::Subscriber<droneMsgsROS::droneSpeedsStamped> drone_sync_vel_ref_sub;
    typedef message_filters::sync_policies::ApproximateTime<droneMsgsROS::droneAutopilotCommand,
    droneMsgsROS::droneDAltitudeCmd,
    droneMsgsROS::droneSpeedsStamped> sync_policy_vel;
    message_filters::Synchronizer<sync_policy_vel> sync_vel;
    void droneAttiAltiVelSyncCallback(droneMsgsROS::droneAutopilotCommandConstPtr atti_msg,
                                      droneMsgsROS::droneDAltitudeCmdConstPtr alti_msg,
                                      droneMsgsROS::droneSpeedsStampedConstPtr vel_msg);


    // Publishers
    ros::Publisher drone_command_publisher;

    //services
    ros::ServiceClient sdk_ctrl_authority_service;
    ros::ServiceClient drone_task_service;

protected:		     
    int counter;
    bool activate_aerostack_control;
    bool use_attitude_mode;
    bool use_mpc_mode;
    bool use_mpc_zvelocity_mode;
    bool use_velocity_mode;
    bool use_external_laser_alt;

public:
    double battery_percent;
    bool update;

};
