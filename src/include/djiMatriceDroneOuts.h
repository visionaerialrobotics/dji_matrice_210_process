//ROS
#include "ros/ros.h"

//math library
#include "math.h"

#include "droneModuleROS.h"
#include "communication_definition.h"

//IMU
#include "sensor_msgs/Imu.h"

//Odometry
#include "nav_msgs/Odometry.h"

//Rotaion Angles
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/TwistStamped.h"

//Battery
#include "droneMsgsROS/battery.h"

//Altitude
#include "droneMsgsROS/droneAltitude.h"
#include "sensor_msgs/Range.h"

//Speeds
#include "droneMsgsROS/vector2Stamped.h"

//Mavros_msgs
#include "mavros_msgs/BatteryStatus.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//Eigen
#include "eigen_conversions/eigen_msg.h"

// low pass filter
#include "control/LowPassFilter.h"
#include "xmlfilereader.h"
#include "control/filtered_derivative_wcb.h"

#include <Eigen/Dense>

#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "sensor_msgs/LaserScan.h"
#include "sensor_msgs/BatteryState.h"
#include "std_msgs/Float32.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"

#include <tf/transform_broadcaster.h>

#include <dji_sdk/SetupCameraStream.h>

#include "geodetic_conv.h"

//#define MBZIRC_AXES_ORIENTATION

class RotationAnglesROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher RotationAnglesPubl;
    ros::Publisher RotationAnglesRadiansPub;
    ros::Publisher ImuDataPubl;
    ros::Publisher RTKpositionPubl;
    ros::Publisher GPSpositionPubl;
    ros::Publisher GeodesicYawPub;

    bool publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs);
    bool publishImuData(const sensor_msgs::Imu &ImuData);


    sensor_msgs::Imu LatestImuMsg;
    ros::Time lastTimeRotation;

    const float OUTLIER_RTK = 30.0; // Meters wrt previous sample
    float prev_gps_x, prev_gps_y, prev_gps_z, prev_rtk_x, prev_rtk_y, prev_rtk_z;
    
    bool init_gimbal_yaw_received_;
    
    double init_gimbal_yaw_;

    //Subscriber
protected:
   ros::Subscriber RotationAnglesSubs;
   void rotationAnglesCallback(const sensor_msgs::Imu &msg);
   bool dji_first_yaw_measurement_, dji_first_yaw_measurement_for_gimbal_;
   bool first_gps_measurement_;
   bool first_rtk_measurement_;

   double dji_first_yaw_, dji_first_yaw_for_gimbal_, dji_first_pitch_, dji_first_roll_;


    // ** Camaron arena ** //
    bool use_geodesic_yaw_;
    double GEO_LAT_1;
    double GEO_LON_1;
    double CART_OFFSET_X_1; // In odom frame of reference
    double CART_OFFSET_Y_1;
    double GEO_LAT_2;
    double GEO_LON_2;
    // **** //

    // ** Futsal arena ** //
    //const double GEO_LAT_1 = 0.0;
    //const double GEO_LON_1 = 0.0;
    //const double CART_OFFSET_X_1 = 0.0;
    //const double CART_OFFSET_Y_1 = 0.0;
    //const double GEO_LAT_2 = 0.0;
    //const double GEO_LON_2 = 0.0;
    // **** //

   ros::Subscriber RTKpositionSubs;
   void RTKpositionCallback(const sensor_msgs::NavSatFixConstPtr& msg);

   ros::Subscriber GPSpositionSubs;
   void GPSpositionCallback(const sensor_msgs::NavSatFixConstPtr& msg);


   //Subscriber
protected:
   //Subscriber
   ros::Subscriber RepublisherVelocitySubs;
   void republisherVelocityCallback(const geometry_msgs::Vector3Stamped &msg);

protected:
  ros::Subscriber GimbalAnglesSubs;
  void gimbalAnglesCallback(const geometry_msgs::Vector3Stamped &msg);
  geometry_msgs::Vector3Stamped gimbal_angles_;

    //RotationAngles msgs
protected:
//    geometry_msgs::Vector3Stamped RotationAnglesMsgs;


    //Constructors and destructors
public:
    RotationAnglesROSModule();
    ~RotationAnglesROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    double roll,pitch,yaw;
    double roll_rad, pitch_rad, yaw_rad;

    Eigen::Quaterniond quaterniond;
    geometry_msgs::Quaternion quaternion;

    geodetic_converter::GeodeticConverter g_geodetic_converter;
    geodetic_converter::GeodeticConverter g_geodetic_converter_gps;

};

//Battery Class
class BatteryROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher BatteryPubl;
    bool publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs);


    //Subscriber
protected:
    ros::Subscriber BatterySubs;
    void batteryCallback(const sensor_msgs::BatteryState &msg);


    //Battery msgs
protected:
//    droneMsgsROS::battery BatteryMsgs;


    //Constructors and destructors
public:
    BatteryROSModule();
    ~BatteryROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

//---Altitude given by the lidarlite --------------//
class AltitudeROSModule : public DroneModule
{
//Publisher
protected:
    ros::Publisher AltitudePub;
    bool publishAltitude(const droneMsgsROS::droneAltitude altitudemsg);
    bool use_external_laser_alt, use_altitude_filtering;

    //Subscriber
protected:
    ros::Subscriber AltitudeSub;
    ros::Subscriber AltitudeFilteredSub;
    void publishAlitudeCallback(const std_msgs::Float32 &msg);
    void publishLaserAlitudeCallback(const sensor_msgs::Range &msg);
    void publishAltitudeFilteredCallback(const geometry_msgs::PoseStamped &msg);
    void republisherVelocityCallback(const geometry_msgs::Vector3Stamped &msg);

protected:
    droneMsgsROS::droneAltitude AltitudeMsg;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    AltitudeROSModule();
    ~AltitudeROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
     bool run();


};

//----------------Local Pose given by Mavros------------//

class LocaPoseROSModule : public DroneModule
{
    //Publisher
protected:
    ros::Publisher localPoseAltitudePublisher;
    bool publishLocalPoseAltitude(const droneMsgsROS::droneAltitude &altitudemsg);
    droneMsgsROS::droneAltitude AltitudeMsg;

    ros::Publisher localPoseSpeedsPublisher;
    ros::Publisher localPosePublisher;
    ros::Publisher localSpeedPublisher;

    bool publishLocalPoseSpeeds(const droneMsgsROS::vector2Stamped &speedsmsg);
    droneMsgsROS::vector2Stamped SpeedsMsg;
    droneMsgsROS::dronePose gazeboPoseMsg;
    droneMsgsROS::droneSpeeds gazeboSpeedsMsg;

    ros::Time lastTimePose;

    //Subscriber
protected:
    ros::Subscriber mavrosLocalPoseSubsriber;
    void localPoseAltitudeCallback(const nav_msgs::Odometry &msg);
    ros::Subscriber mavrosLocalSpeedsSubsriber;
    void localSpeedsCallback(const geometry_msgs::TwistStamped &msg);
    void localSpeedsCallbackGazebo(const nav_msgs::Odometry &msg);

protected:
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    LocaPoseROSModule();
    ~LocaPoseROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    std::string mav_name;
    int droneSwarmId;

   // double roll,pitch,yaw;

};
