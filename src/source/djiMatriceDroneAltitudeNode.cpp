
#include <iostream>


//ROS
#include "ros/ros.h"

//Pixhawkouts
#include "djiMatriceDroneOuts.h"

//Comunications
#include "communication_definition.h"


using namespace std;

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, "droneAltitude");
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting droneAltitude"<<endl;

    //Vars
    AltitudeROSModule MyaltitudeROSModule;
    MyaltitudeROSModule.open(n,"droneAltitude");

    try
    {
        //Read messages
        ros::spin();
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}
